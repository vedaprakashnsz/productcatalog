package com.trezi.productcatalog.exception;

/**
 * @author Mourya Balla
 * @project collaboration-service
 * @CreatedOn 15-02-2019
 */
public class TZIConversionException extends RuntimeException {
    public TZIConversionException(String message){
        super(message);
    }

    public TZIConversionException(String message, Throwable cause) {
        super(message, cause);
    }
}
