package com.trezi.productcatalog.exception;


import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;

public class CatalogException extends RuntimeException{
    String cleanedMessage;
    HttpStatus returnStatus;

    public String getCleanedMessage() { return cleanedMessage; }

    public CatalogException (String message) {
        super(message);
        returnStatus = HttpStatus.ACCEPTED;
    }

    public CatalogException (String message, HttpStatus  retStatus) {
        super(message);
        returnStatus = retStatus;
    }

    public CatalogException (Throwable throwable){
        super(throwable);

        if (throwable instanceof JsonProcessingException) {
            cleanedMessage = throwable.getMessage();
            returnStatus = HttpStatus.BAD_REQUEST;
        } else if (throwable instanceof RestClientException) {
            if (throwable instanceof HttpStatusCodeException) {
                cleanedMessage = ((HttpStatusCodeException)throwable).getResponseBodyAsString();
                returnStatus = ((HttpStatusCodeException) throwable).getStatusCode();
            } else {
                cleanedMessage = throwable.getMessage();
                returnStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            }
        }
    }

    public HttpStatus getreturnStatus() {
        return returnStatus;
    }
}

