package com.trezi.productcatalog.domain;


import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "master_attributes")
public class MasterAttributes {

    @Id
    private Integer id;
    private String name;
    private String label;
    private String variableType;
    private String attrSection;
    private Boolean isFilter;

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getLabel() {
        return label;
    }

    public String getVariableType() {
        return variableType;
    }

    public String getAttrSection() {
        return attrSection;
    }

    public Boolean getFilter() {
        return isFilter;
    }
}
