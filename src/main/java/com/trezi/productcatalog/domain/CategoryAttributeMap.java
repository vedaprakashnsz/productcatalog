package com.trezi.productcatalog.domain;


import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.logging.Filter;

@Entity
@Table(name = "cat_attr_map")
public class CategoryAttributeMap {

    @Id
    private Integer id;
    private Integer catId;
    private Integer attrId;
    private Boolean isMandatory;
    private Boolean isFilter;


    public Integer getId() {
        return id;
    }

    public Integer getCatId() {
        return catId;
    }

    public Integer getAttrId() {
        return attrId;
    }

    public Boolean getMandatory() {
        return isMandatory;
    }

    public Boolean getFilter() {
        return isFilter;
    }
}


