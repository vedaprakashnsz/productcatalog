package com.trezi.productcatalog.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "draft_item")
public class DraftItem {
    /**
     * CREATE TABLE draft_item
     * (
     * 	[id] INT NOT NULL PRIMARY KEY,
     *     [name] NVARCHAR(200) NOT NULL,
     *     [client_id] NVARCHAR(50) NOT NULL,
     *     [created_on] DATE NULL,
     *     [created_by] NVARCHAR(50) NULL,
     *     [geometry_link] NVARCHAR(500) NULL,
     *     [edited_on] DATE NULL,
     *     [edited_by] NVARCHAR(50) NULL,
     *     [cat_id] INT NOT NULL,
     *     [thumbnail_link] NVARCHAR(500) NULL
     * )
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DRAFT_ITEM_SEQ")
    @SequenceGenerator(sequenceName = "DRAFT_ITEM_SEQ", allocationSize = 1, name = "DRAFT_ITEM_SEQ")
    private Integer id;
    private String name;
    private String clientId;
    private Date createdOn;
    private String createdBy;
    private String geometryLink;
    private Date editedOn;
    private String editedBy;
    private Integer catId;
    private String thumbnailLink;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getGeometryLink() {
        return geometryLink;
    }

    public void setGeometryLink(String geometryLink) {
        this.geometryLink = geometryLink;
    }

    public Date getEditedOn() {
        return editedOn;
    }

    public void setEditedOn(Date editedOn) {
        this.editedOn = editedOn;
    }

    public String getEditedBy() {
        return editedBy;
    }

    public void setEditedBy(String editedBy) {
        this.editedBy = editedBy;
    }

    public Integer getCatId() {
        return catId;
    }

    public void setCatId(Integer catId) {
        this.catId = catId;
    }

    public String getThumbnailLink() {
        return thumbnailLink;
    }

    public void setThumbnailLink(String thumbnailLink) {
        this.thumbnailLink = thumbnailLink;
    }
}
