package com.trezi.productcatalog.domain;

import javax.persistence.*;

@Entity
@Table(name = "category_def")
public class CategoryDef {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer id;
    private String name;
    private Integer parentId;
    private String catKey;

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getParentId() {
        return parentId;
    }

    public String getCatKey() {
        return catKey;
    }
}
