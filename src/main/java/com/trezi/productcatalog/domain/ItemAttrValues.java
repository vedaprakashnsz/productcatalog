package com.trezi.productcatalog.domain;

import javax.persistence.*;

@Entity
@Table(name = "item_attr_values")
public class ItemAttrValues {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ITEM_ATTR_VALUE_SEQ")
    @SequenceGenerator(sequenceName = "ITEM_ATTR_VALUE_SEQ", allocationSize = 1, name = "ITEM_ATTR_VALUE_SEQ")
    private Integer id;
    private Integer itemId;
    private Integer attrId;
    private String attrValue;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public Integer getAttrId() {
        return attrId;
    }

    public void setAttrId(Integer attrId) {
        this.attrId = attrId;
    }

    public String getAttrValue() {
        return attrValue;
    }

    public void setAttrValue(String attrValue) {
        this.attrValue = attrValue;
    }
}
