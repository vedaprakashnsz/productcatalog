package com.trezi.productcatalog.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "item")
public class Item {

    /**
     * CREATE TABLE item
     * (
     * 	[id] INT NOT NULL PRIMARY KEY,
     * [name] NVARCHAR(200) NOT NULL,
     * [client_id] NVARCHAR(50) NULL,
     * [created_on] DATE NULL,
     * [created_by] NVARCHAR(50) NULL,
     * [geometry_link] NVARCHAR(500) NULL,
     * [thumbnail_link] NVARCHAR(500) NULL,
     * [edited_on] DATE NULL,
     * [edited_by] NVARCHAR(50) NULL,
     * [cat_id] INT NOT NULL,
     * [is_published] BIT NOT NULL,
     * [is_active] BIT NOT NULL
     * )
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ITEM_SEQ")
    @SequenceGenerator(sequenceName = "ITEM_SEQ", allocationSize = 1, name = "ITEM_SEQ")
    private Integer id;
    private String name;
    private Integer catId;
    private Integer clientId;
    private Date createdOn;
    private String createdBy;
    private String geometryLink;
    private String thumbnailLink;
    private Date editedOn;
    private String editedBy;
    private Boolean isPublished;
    private Boolean isActive;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCatId() {
        return catId;
    }

    public void setCatId(Integer catId) {
        this.catId = catId;
    }

    public Integer getClientId() {
        return clientId;
    }

    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getGeometryLink() {
        return geometryLink;
    }

    public void setGeometryLink(String geometryLink) {
        this.geometryLink = geometryLink;
    }

    public String getThumbnailLink() {
        return thumbnailLink;
    }

    public void setThumbnailLink(String thumbnailLink) {
        this.thumbnailLink = thumbnailLink;
    }

    public Date getEditedOn() {
        return editedOn;
    }

    public void setEditedOn(Date editedOn) {
        this.editedOn = editedOn;
    }

    public String getEditedBy() {
        return editedBy;
    }

    public void setEditedBy(String editedBy) {
        this.editedBy = editedBy;
    }

    public Boolean getPublished() {
        return isPublished;
    }

    public void setPublished(Boolean published) {
        isPublished = published;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }
}
