package com.trezi.productcatalog.utility;

import com.trezi.productcatalog.exception.TZIConversionException;
import feign.hystrix.FallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author Mourya Balla
 * @project collaboration-service
 * @CreatedOn 08-03-2019
 */
@Configuration
@FeignClient(
        name = "convertor-service",
        url = "${feign.convertor-service.url}",
        //url = "http://localhost:7072/convertorService",
        fallbackFactory = ConvertorServiceFeignProxy.ConvertorServiceFallbackFactory.class
)
public interface ConvertorServiceFeignProxy {

    @GetMapping(value = "/convertSkpToTZI", consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<?> convertSkpToTZI(@RequestParam String blobLink);

    @GetMapping(value = "/convertRvtToTZI", consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<?> convertRvtToTZI(@RequestParam String blobLink);

    @GetMapping(value = "/actuator/health")
    String healthCheck();


    @Component
    public static class ConvertorServiceFallbackFactory implements FallbackFactory<ConvertorServiceFeignProxy> {
        @Override
        public ConvertorServiceFeignProxy create(Throwable cause) {
            return new ConvertorServiceFeignProxy() {
                @Override
                public ResponseEntity<?> convertSkpToTZI(String blobLink) {
                    throw new TZIConversionException("SKP-->TZI :: Convertor service Exception:",cause);
                    //return null;
                }

                @Override
                public ResponseEntity<?> convertRvtToTZI(String blobLink) {
                    throw new TZIConversionException("RVT-->TZI :: Convertor service Exception:",cause);
                }

                @Override
                public String healthCheck() {
                    throw new TZIConversionException("RVT-->TZI :: Convertor service Exception:",cause);
                }

            };
        }
    }
}
