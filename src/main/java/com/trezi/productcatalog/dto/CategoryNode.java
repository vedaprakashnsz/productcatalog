package com.trezi.productcatalog.dto;

import java.util.List;

public class CategoryNode {

    Integer id;
    String name;
    List<CategoryNode> childNodes;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<CategoryNode> getChildNodes() {
        return childNodes;
    }

    public void setChildNodes(List<CategoryNode> childNodes) {
        this.childNodes = childNodes;
    }
}
