package com.trezi.productcatalog.dto;

import java.util.List;

public class CategoryCountNode {
    Integer id;
    String name;
    List<CategoryCountNode> childNodes;
    Integer count;
    String url;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<CategoryCountNode> getChildNodes() {
        return childNodes;
    }

    public void setChildNodes(List<CategoryCountNode> childNodes) {
        this.childNodes = childNodes;
    }
}
