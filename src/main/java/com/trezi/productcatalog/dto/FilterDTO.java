package com.trezi.productcatalog.dto;

import java.util.ArrayList;
import java.util.List;

public class FilterDTO {

    Integer id;
    String name;
    Integer count;
//    String url;

    List<FilterItemDTO> filterItems = new ArrayList<FilterItemDTO>();

    public List<FilterItemDTO> getFilterItems() {
        return filterItems;
    }

    public void setFilterItems(List<FilterItemDTO> filterItems) {
        this.filterItems = filterItems;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
