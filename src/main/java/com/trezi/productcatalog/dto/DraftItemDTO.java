package com.trezi.productcatalog.dto;

public class DraftItemDTO {
    Integer itemId;
    String name;
    String geometryLink;
    String thumbnailLink;
    Integer catId;

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGeometryLink() {
        return geometryLink;
    }

    public void setGeometryLink(String geometryLink) {
        this.geometryLink = geometryLink;
    }

    public String getThumbnailLink() {
        return thumbnailLink;
    }

    public void setThumbnailLink(String thumbnailLink) {
        this.thumbnailLink = thumbnailLink;
    }

    public Integer getCatId() {
        return catId;
    }

    public void setCatId(Integer catId) {
        this.catId = catId;
    }
}
