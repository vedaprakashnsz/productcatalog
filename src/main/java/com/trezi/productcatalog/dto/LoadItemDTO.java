package com.trezi.productcatalog.dto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LoadItemDTO {

    String name;
    Integer id;
    String geometryLink;
    String thumbnailLink;
    String categoryName;
    Integer categoryId;

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    Map<String, List<ItemAttributeValueDTO>> itemAttributes = new HashMap<String, List<ItemAttributeValueDTO>>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGeometryLink() {
        return geometryLink;
    }

    public void setGeometryLink(String geometryLink) {
        this.geometryLink = geometryLink;
    }

    public String getThumbnailLink() {
        return thumbnailLink;
    }

    public void setThumbnailLink(String thumbnailLink) {
        this.thumbnailLink = thumbnailLink;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Map<String, List<ItemAttributeValueDTO>> getItemAttributes() {
        return itemAttributes;
    }

    public void setItemAttributes(Map<String, List<ItemAttributeValueDTO>> itemAttributes) {
        this.itemAttributes = itemAttributes;
    }
}
