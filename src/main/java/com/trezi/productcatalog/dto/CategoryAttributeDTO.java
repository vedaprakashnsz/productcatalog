package com.trezi.productcatalog.dto;

public class CategoryAttributeDTO {

    Integer id;
    Integer catId;
    Integer attrId;
    String name;
    String label;
    String variableType;
    Boolean isMandatory;
    String attrSection;
    Boolean isFilter;

    public CategoryAttributeDTO(Integer id, Integer catId, Integer attrId, String name, String label, String variableType, Boolean isMandatory, String  attrSection, Boolean isFilter){
        this.id = id;
        this.catId = catId;
        this.attrId = attrId;
        this.name = name;
        this.label = label;
        this.variableType = variableType;
        this.isMandatory = isMandatory;
        this.attrSection = attrSection;
        this.isFilter = isFilter;
    }

    public CategoryAttributeDTO(){

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCatId() {
        return catId;
    }

    public void setCatId(Integer catId) {
        this.catId = catId;
    }

    public Integer getAttrId() {
        return attrId;
    }

    public void setAttrId(Integer attrId) {
        this.attrId = attrId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getVariableType() {
        return variableType;
    }

    public void setVariableType(String variableType) {
        this.variableType = variableType;
    }

    public Boolean getMandatory() {
        return isMandatory;
    }

    public void setMandatory(Boolean mandatory) {
        isMandatory = mandatory;
    }

    public String getAttrSection() {
        return attrSection;
    }

    public void setAttrSection(String attrSection) {
        this.attrSection = attrSection;
    }

    public Boolean getFilter() {
        return isFilter;
    }

    public void setFilter(Boolean filter) {
        isFilter = filter;
    }
}
