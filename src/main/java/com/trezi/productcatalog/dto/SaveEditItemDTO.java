package com.trezi.productcatalog.dto;

import java.util.List;

public class SaveEditItemDTO {

    Integer itemId;
    String name;
    List<ItemAttributeValueDTO> itemAttributeValueDTOS;
    String geometryLink;
    String thumbnailLink;
    String catId;

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ItemAttributeValueDTO> getItemAttributeValueDTOS() {
        return itemAttributeValueDTOS;
    }

    public void setItemAttributeValueDTOS(List<ItemAttributeValueDTO> itemAttributeValueDTOS) {
        this.itemAttributeValueDTOS = itemAttributeValueDTOS;
    }

    public String getGeometryLink() {
        return geometryLink;
    }

    public void setGeometryLink(String geometryLink) {
        this.geometryLink = geometryLink;
    }

    public String getThumbnailLink() {
        return thumbnailLink;
    }

    public void setThumbnailLink(String thumbnailLink) {
        this.thumbnailLink = thumbnailLink;
    }
}
