package com.trezi.productcatalog.repository;

import com.trezi.productcatalog.domain.CategoryDef;
import com.trezi.productcatalog.services.CatalogService;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface CategoryDefRepository extends JpaRepository<CategoryDef, Integer> {
    CategoryDef findByName(String name);
    CategoryDef findByParentId(Integer parentId);
}
