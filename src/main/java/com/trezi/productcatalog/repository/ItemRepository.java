package com.trezi.productcatalog.repository;

import com.trezi.productcatalog.domain.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ItemRepository extends PagingAndSortingRepository<Item, Integer> {
    public List findAllByClientId(Integer clientId);

    @Query("select it.id, it.name, it.geometryLink, it.thumbnailLink,cd.name, ma.name, itav.attrId, itav.attrValue, ma.attrSection, cd.id, ma.variableType from Item it, ItemAttrValues itav, MasterAttributes ma, CategoryDef cd  where it.id = itav.itemId and ma.id = itav.attrId and it.catId = cd.id and  it.id =:itemId order by ma.attrSection")
    public List fetchItemDetails(@Param("itemId") Integer itemId);

    @Query("select count(cat.name) , cat.name from Item i , CategoryDef cat where cat.id = i.catId  and i.clientId =:clientId group by cat.name")
    public List getCounts(@Param("clientId") Integer clientId);

    @Query("select i from Item i where clientId =:clientId and catId IN (:catIds)")
    public List findAllByClientIdAndCatId(@Param("clientId") Integer clientId, @Param("catIds") List<Integer> catIds);

    @Query("select it from Item it where it.id in (select distinct(iav.itemId) from ItemAttrValues iav, MasterAttributes ma where ma.id = iav.attrId and iav.attrId =:attrId and iav.attrValue IN (:attrValues)) and it.clientId =:clientId")
    public List getAllByAttrTypeAndValue(@Param("clientId") Integer clientId, @Param("attrId") Integer  attrId, @Param("attrValues") List<String> attrValues);

    @Query("select distinct(ma.id), ma.name from MasterAttributes ma, CategoryAttributeMap cam where ma.id = cam.attrId and cam.isFilter = 1")
    public List getFilters();

    @Query("select distinct(ma.id), ma.name from MasterAttributes ma, CategoryAttributeMap cam where ma.id = cam.attrId and cam.isFilter = 1 and cam.catId =:catId")
    public List getFilters(@Param("catId") Integer catId);

    @Query("select (iav.attrValue), count(distinct iav.itemId) from MasterAttributes ma , CategoryAttributeMap cam, ItemAttrValues iav where ma.id = cam.attrId and cam.isFilter = 1 and iav.attrId = ma.id and iav.attrId =:attrId group by iav.attrId,iav.attrValue")
    public List getDistinctValuesPerFilterAttribute(@Param("attrId") Integer attrId);
}
