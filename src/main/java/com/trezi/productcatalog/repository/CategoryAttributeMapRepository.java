package com.trezi.productcatalog.repository;

import com.trezi.productcatalog.domain.CategoryAttributeMap;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CategoryAttributeMapRepository extends JpaRepository<CategoryAttributeMap,Integer> {

    List<CategoryAttributeMap> findAllByCatId(Integer catId);

    @Query("select cam.id as id,cam.catId as catId, cam.attrId as attrId, ma.name as name, ma.label as label, ma.variableType as variableType, cam.isMandatory as isMandatory, ma.attrSection as attrSection, cam.isFilter as isFilter from MasterAttributes ma, CategoryAttributeMap cam where cam.attrId = ma.id and cam.catId =:categoryId order by cam.catId")
   public List fetchAtrributeInfoByCategory(@Param("categoryId") Integer categoryId);
}
