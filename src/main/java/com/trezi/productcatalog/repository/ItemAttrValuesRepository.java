package com.trezi.productcatalog.repository;

import com.trezi.productcatalog.domain.ItemAttrValues;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ItemAttrValuesRepository extends JpaRepository<ItemAttrValues, Integer> {

    @Modifying
    @Query("delete from ItemAttrValues where itemId =:itemId")
    Integer deleteItemAttriButes(@Param("itemId") Integer itemId);
}
