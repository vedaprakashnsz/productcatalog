package com.trezi.productcatalog.repository;

import com.trezi.productcatalog.domain.DraftItem;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DraftItemRepository extends JpaRepository<DraftItem,Integer> {

}
