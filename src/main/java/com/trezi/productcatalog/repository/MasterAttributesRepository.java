package com.trezi.productcatalog.repository;

import com.trezi.productcatalog.domain.MasterAttributes;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MasterAttributesRepository extends JpaRepository<MasterAttributes,Integer> {


}
