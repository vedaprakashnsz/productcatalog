package com.trezi.productcatalog.controllers;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.trezi.productcatalog.dto.CategoryNode;
import com.trezi.productcatalog.domain.CategoryDef;
import com.trezi.productcatalog.domain.DraftItem;
import com.trezi.productcatalog.dto.*;
import com.trezi.productcatalog.exception.CatalogException;
import com.trezi.productcatalog.services.CatalogService;
import com.trezi.productcatalog.services.UtilityService;
import com.trezi.productcatalog.utility.RestResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@CrossOrigin(origins = {"http://test.trezi.com", "http://localhost:8080"}, maxAge = 6000, allowCredentials = "false")
@RestController
public class CatalogController {

    private static final Logger logger = LoggerFactory.getLogger(CatalogController.class.getName());

    @Inject
    RestResponse restResponse;

    @Inject
    CatalogService catalogService;

    @Inject
    UtilityService utilityService;



    @RequestMapping(value = "/categories", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> getList() {
        try{
            List<CategoryDef> categories = catalogService.getCategories();
            List<CategoryNode> categoryNodes = utilityService.getCategoryMap(categories);
            return restResponse.successWithData(categoryNodes);
        }
        catch (Exception e){
            logger.error("Error in getting categories list");
            throw new CatalogException("Error in getting categories");
        }

    }


    @RequestMapping(value = "/categories/counts", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> getCategoryCounts(@RequestParam("clientId") Integer clientId) {
        try{
            List<CategoryDef> categories = catalogService.getCategories();
            List counts = catalogService.getCategoryCounts(clientId);
            List<CategoryCountNode> categoryCounts = utilityService.getCategoryCountMap(categories, counts, clientId);
            return restResponse.successWithData(categoryCounts);
        }
        catch (Exception e){
            logger.error("Error in getting categories list");
            throw new CatalogException("Error in getting categories");
        }

    }

    @RequestMapping(value = "/category/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> getDataForCategory(@PathVariable Integer id) {
        try{
            Map attributes = catalogService.getCategoryDataById(id);
            return restResponse.successWithData(attributes);
        }
        catch (Exception e){
            logger.error("Error in fetching meta data for category : " + e.getMessage());
            throw new CatalogException("Error fetching data for selected category");
        }
    }

    @RequestMapping(value = "/catalog/uploadGeometry", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<?> uploadGeometry(@RequestPart("geometryFile") MultipartFile geometryFile,
                                             @RequestPart String  categoryId,
                                             @RequestPart String createdBy,
                                             @RequestPart String clientId,
                                             @RequestPart String itemId,
                                             @RequestPart String itemName
                                            //@RequestPart String baseItemId) {
    ){

        DraftItem draftItem = catalogService.uploadGeometryFile(geometryFile, Integer.parseInt(categoryId), itemName,
                (null != itemId && !itemId.equalsIgnoreCase("null") ? Integer.parseInt(itemId) : null), clientId, createdBy);
        DraftItemDTO draftItemDTO = new DraftItemDTO();
        draftItemDTO.setItemId(draftItem.getId());
        draftItemDTO.setName(draftItem.getName());
        draftItemDTO.setGeometryLink(draftItem.getGeometryLink());
        draftItemDTO.setThumbnailLink(draftItem.getThumbnailLink());
        draftItemDTO.setCatId(draftItem.getCatId());
        ObjectMapper objectMapper = new ObjectMapper();
        Map json = objectMapper.convertValue(draftItemDTO, Map.class);
        return restResponse.successWithData(json);
    }

    @RequestMapping(value = "/catalog/uploadThumbnail", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<?> uploadThumbNail(@RequestPart("thumbnailFile") MultipartFile thumbnailFile,
                                             @RequestPart String  categoryId,
                                             @RequestPart String createdBy,
                                             @RequestPart String clientId,
                                             @RequestPart String itemId,
                                             @RequestPart String itemName) {

        DraftItem draftItem = catalogService.uploadThumbnail(thumbnailFile, Integer.parseInt(categoryId), itemName,
                (null != itemId && !itemId.equalsIgnoreCase("null") ? Integer.parseInt(itemId) : null), clientId, createdBy);
        DraftItemDTO draftItemDTO = new DraftItemDTO();
        draftItemDTO.setItemId(draftItem.getId());
        draftItemDTO.setName(draftItem.getName());
        draftItemDTO.setGeometryLink(draftItem.getGeometryLink());
        draftItemDTO.setThumbnailLink(draftItem.getThumbnailLink());
        draftItemDTO.setCatId(draftItem.getCatId());
        ObjectMapper objectMapper = new ObjectMapper();
        Map json = objectMapper.convertValue(draftItemDTO, Map.class);
        return restResponse.successWithData(json);
    }

    @RequestMapping(value = "/item", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<?> saveItem(@RequestBody SaveEditItemDTO saveEditItemDTO) {
        logger.info(" the dto : " + saveEditItemDTO);
        catalogService.saveItem(saveEditItemDTO);
        return restResponse.success("Item Created");
    }

    @RequestMapping(value = "/items", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> getItems(@RequestParam("clientId") Integer clientId, @RequestParam("isPublished") Optional<Boolean> isPublished) {
        List<ListItemDTO> listItems = catalogService.getItemsList(clientId, (isPublished.isPresent())?isPublished.get():null);
        return restResponse.successWithData(listItems);
    }


//    @RequestMapping(value = "/item", method = RequestMethod.GET)
//    @ResponseBody
//    public ResponseEntity<?> getItem(@RequestParam("key") String key,@RequestParam("value") String value){
//        String keyDecoded = utilityService.decode(key);
//        String valueDecoded = utilityService.decode(value);
//        return restResponse.success("Sent");
//    }

    @RequestMapping(value = "/item/publish", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<?> publishItem(@RequestBody PublishItemDTO publishItemDTO) {
        logger.info(" the dto : " + publishItemDTO);
        catalogService.publishItem(publishItemDTO);
        return restResponse.success("Item Published");
    }

    @RequestMapping(value = "/item", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> getItemDetails(@RequestParam("id") String id){
        LoadItemDTO loadItemDTO = catalogService.getItemDetails(Integer.parseInt(id));
        ObjectMapper objectMapper = new ObjectMapper();
        Map json = objectMapper.convertValue(loadItemDTO, Map.class);
        return restResponse.successWithData(json);
    }

    @RequestMapping(value = "/item", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<?> deleteItem(@RequestParam("id") String id){
        Boolean isDeleted = catalogService.deleteItem(Integer.parseInt(id));
        return restResponse.success("Item Deleted");
    }

//    @RequestMapping(value = "/sample", method = RequestMethod.GET)
//    @ResponseBody
//    public ResponseEntity<?> sample(@RequestParam("id") String id){
//        Map<String,String> paramsMap = new HashMap<>();
//        paramsMap.put("code","OfxYfDHKmZzajaRqrMhlaXog9hjNqb2Nk/4ucQ1yJHNc6uZiDZzUVA==");
//        paramsMap.put("inputFile","https://testfuncf82a2.blob.core.windows.net/skptotzistorage/3_tiny_apartments.skp");
//        utilityService.makeGETAPICall("https://skptotzitwo120190307011553.azurewebsites.net/api/func64bit1?code=OfxYfDHKmZzajaRqrMhlaXog9hjNqb2Nk/4ucQ1yJHNc6uZiDZzUVA==&inputFile=https://testfuncf82a2.blob.core.windows.net/skptotzistorage/3_tiny_apartments.skp",null);
//        return restResponse.success("");
//    }

    @RequestMapping(value = "/item/filter", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> getFilteredItems(@RequestParam("clientId") Integer clientId, @RequestParam("type") String type,
                                              @RequestParam("key") Integer key, @RequestParam("value") String value){
        List<ListItemDTO> listItems = new ArrayList<ListItemDTO>();
        if(null != type && type.equalsIgnoreCase("category")){
                listItems = catalogService.getItemsListByCategoryId(clientId, value);
        }
        else if(null != type && type.equalsIgnoreCase("attribute")){
            listItems = catalogService.getItemsListByAttributes(clientId, key, value);
        }
        return restResponse.successWithData(listItems);
    }

    @RequestMapping(value = "/filters", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> getFilters(@RequestParam("clientId") Integer clientId,
                                              @RequestParam("catId") Integer catId){
        List filters = catalogService.getFilters(clientId, catId);
        return restResponse.successWithData(filters);
    }
}
