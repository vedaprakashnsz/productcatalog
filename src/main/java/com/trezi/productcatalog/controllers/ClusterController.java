package com.trezi.productcatalog.controllers;


import com.trezi.productcatalog.domain.CategoryDef;
import com.trezi.productcatalog.dto.CategoryNode;
import com.trezi.productcatalog.exception.CatalogException;
import com.trezi.productcatalog.services.CatalogService;
import com.trezi.productcatalog.services.UtilityService;
import com.trezi.productcatalog.utility.RestResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.List;

@CrossOrigin(origins = {"http://test.trezi.com", "http://localhost:8080", "https://app.trezi.com"}, maxAge = 6000, allowCredentials = "false")
@RestController
public class ClusterController {

    private static final Logger logger = LoggerFactory.getLogger(ClusterController.class.getName());

    @Inject
    RestResponse restResponse;

    @Inject
    CatalogService catalogService;

    @Inject
    UtilityService utilityService;


    @RequestMapping(value = "/categories", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> getList() {
        try{

            return restResponse.success(null);
        }
        catch (Exception e){
            logger.error("Error in getting cluster list");
            throw new CatalogException("Error in getting cluster");
        }

    }
}
