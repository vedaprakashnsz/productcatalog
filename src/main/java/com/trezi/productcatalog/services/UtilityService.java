package com.trezi.productcatalog.services;

import com.trezi.productcatalog.dto.*;
import com.trezi.productcatalog.domain.CategoryDef;
import com.trezi.productcatalog.repository.CategoryDefRepository;
import com.trezi.productcatalog.repository.ItemRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.inject.Inject;
import java.util.*;


@Service
public class UtilityService {

    private static final Logger logger = LoggerFactory.getLogger(UtilityService.class.getName());

    @Inject
    CategoryDefRepository categoryDefRepository;

    @Inject
    RestTemplate restTemplate;

    @Inject
    ItemRepository itemRepository;

    public List<CategoryNode> getCategoryMap(List<CategoryDef> categories){
        List<CategoryNode> categoryNodes = new ArrayList<CategoryNode>();
        for(CategoryDef categoryDef : categories){
            if(categoryDef.getParentId() == null){
                CategoryNode categoryNode = new CategoryNode();
                categoryNode.setId(categoryDef.getId());
                categoryNode.setName(categoryDef.getName());
                categoryNode.setChildNodes(new ArrayList<CategoryNode>());
                categoryNodes.add(categoryNode);
            }
            else{
                Integer parentId = categoryDef.getParentId();
                Boolean isFirstLevel = false;
                for(CategoryNode categoryNode : categoryNodes){
                    if(categoryNode.getId() == parentId){
                        isFirstLevel = true;
                        List<CategoryNode> categoryNodes1 = categoryNode.getChildNodes();
                        CategoryNode childNode = new CategoryNode();
                        childNode.setName(categoryDef.getName());
                        childNode.setId(categoryDef.getId());
                        childNode.setChildNodes(new ArrayList<CategoryNode>());
                        categoryNodes1.add(childNode);
                        categoryNode.setChildNodes(categoryNodes1);
                    }
                }
                if(!isFirstLevel){
                    CategoryDef categoryDef1 = categoryDefRepository.findById(parentId).get();
                    if(categoryDef1 != null){
                        for(CategoryNode categoryNode : categoryNodes){
                            if(categoryNode.getId() == categoryDef1.getParentId()){
                                List<CategoryNode> childrenOfGrandParent = categoryNode.getChildNodes();
                                for(CategoryNode parentNode : childrenOfGrandParent){
                                    if(parentNode.getId() == parentId){
                                        List<CategoryNode> parentsChildren = parentNode.getChildNodes();
                                        CategoryNode newChildNode = new CategoryNode();
                                        newChildNode.setName(categoryDef.getName());
                                        newChildNode.setId(categoryDef.getId());
                                        newChildNode.setChildNodes(new ArrayList<CategoryNode>());
                                        parentsChildren.add(newChildNode);
                                        parentNode.setChildNodes(parentsChildren);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        logger.info(" the categoryNode : " + categoryNodes);
        return categoryNodes;
    }


    public List<CategoryCountNode> getCategoryCountMap(List<CategoryDef> categories, List counts, Integer clientId){

        List<CategoryCountNode> categoryNodes = new ArrayList<CategoryCountNode>();
        Map<String, Integer> countsMap = new HashMap<>();
        for(Object row : counts){
            Object[] objects = null;
            System.out.println(" in row : " + row);
            if (row.getClass().isArray()) {
                objects = (Object[]) row;
            }
            countsMap.put((objects[1].toString()),Integer.parseInt(objects[0].toString()));
        }
        for(CategoryDef categoryDef : categories){
            if(categoryDef.getParentId() == null){
                CategoryCountNode categoryNode = new CategoryCountNode();
                categoryNode.setId(categoryDef.getId());
                categoryNode.setName(categoryDef.getName());
                categoryNode.setCount((countsMap.keySet().contains(categoryDef.getName()))?countsMap.get(categoryDef.getName()):0);
                categoryNode.setUrl("/item/filter?clientId=" + clientId + "&type=category&key=catId&value=" + categoryDef.getId());
                categoryNode.setChildNodes(new ArrayList<CategoryCountNode>());
                categoryNodes.add(categoryNode);
            }
            else{
                Integer parentId = categoryDef.getParentId();
                Boolean isFirstLevel = false;
                for(CategoryCountNode categoryNode : categoryNodes){
                    if(categoryNode.getId() == parentId){
                        isFirstLevel = true;
                        List<CategoryCountNode> categoryNodes1 = categoryNode.getChildNodes();
                        CategoryCountNode childNode = new CategoryCountNode();
                        childNode.setName(categoryDef.getName());
                        childNode.setCount((countsMap.keySet().contains(categoryDef.getName()))?countsMap.get(categoryDef.getName()):0);
                        childNode.setUrl("/item/filter?clientId=" + clientId + "&type=category&key=catId&value=" + categoryDef.getId());
                        childNode.setId(categoryDef.getId());
                        childNode.setChildNodes(new ArrayList<CategoryCountNode>());
                        categoryNodes1.add(childNode);
                        categoryNode.setChildNodes(categoryNodes1);
                        categoryNode.setCount(categoryNode.getCount() + childNode.getCount());
                    }
                }
                if(!isFirstLevel){
                    CategoryDef categoryDef1 = categoryDefRepository.findById(parentId).get();
                    if(categoryDef1 != null){
                        for(CategoryCountNode categoryNode : categoryNodes){
                            if(categoryNode.getId() == categoryDef1.getParentId()){
                                List<CategoryCountNode> childrenOfGrandParent = categoryNode.getChildNodes();
                                for(CategoryCountNode parentNode : childrenOfGrandParent){
                                    if(parentNode.getId() == parentId){
                                        List<CategoryCountNode> parentsChildren = parentNode.getChildNodes();
                                        CategoryCountNode newChildNode = new CategoryCountNode();
                                        newChildNode.setName(categoryDef.getName());
                                        newChildNode.setCount((countsMap.keySet().contains(categoryDef.getName()))?countsMap.get(categoryDef.getName()):0);
                                        newChildNode.setUrl("/item/filter?clientId=" + clientId + "&type=category&key=catId&value=" + categoryDef.getId());
                                        newChildNode.setId(categoryDef.getId());
                                        newChildNode.setChildNodes(new ArrayList<CategoryCountNode>());
                                        parentsChildren.add(newChildNode);
                                        parentNode.setChildNodes(parentsChildren);
                                        parentNode.setCount(parentNode.getCount() + newChildNode.getCount());
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        logger.info(" the categoryNode : " + categoryNodes);
        return categoryNodes;
    }

    public Map<String,List<CategoryAttributeDTO>> populateCatalogAttributes(List queryResults){
        List<CategoryAttributeDTO> catalogAttributes = new ArrayList<CategoryAttributeDTO>();
        LinkedHashMap<String,List<CategoryAttributeDTO>> attrSections = initializeAttrSections();
        for(Object row : queryResults){
            Object[] objects = null;
            System.out.println(" in row : " + row);
            CategoryAttributeDTO categoryAttributeDTO = new CategoryAttributeDTO();
            if (row.getClass().isArray()) {
                objects = (Object[]) row;
            }
            categoryAttributeDTO.setId(Integer.parseInt(objects[0].toString()));
            categoryAttributeDTO.setCatId(Integer.parseInt(objects[1].toString()));
            categoryAttributeDTO.setAttrId(Integer.parseInt(objects[2].toString()));
            categoryAttributeDTO.setName((objects[3].toString()));
            categoryAttributeDTO.setLabel((objects[4].toString()));
            categoryAttributeDTO.setVariableType((objects[5].toString()));
            categoryAttributeDTO.setMandatory(Boolean.getBoolean(objects[6].toString()));
            categoryAttributeDTO.setAttrSection((objects[7].toString()));
            categoryAttributeDTO.setFilter(Boolean.getBoolean(objects[8].toString()));
            catalogAttributes.add(categoryAttributeDTO);
            if(attrSections.keySet().contains(categoryAttributeDTO.getAttrSection())){
                List<CategoryAttributeDTO> existingList = attrSections.get(categoryAttributeDTO.getAttrSection());
                existingList.add(categoryAttributeDTO);
                attrSections.put(categoryAttributeDTO.getAttrSection(),existingList);
            }
            else {
                List<CategoryAttributeDTO> newList = new ArrayList<CategoryAttributeDTO>();
                newList.add(categoryAttributeDTO);
                attrSections.put(categoryAttributeDTO.getAttrSection(),newList);
            }
        }
        List<String> removeKeys = new ArrayList<>();
        for(String attrSection : attrSections.keySet()){
            if(((List)attrSections.get(attrSection)).size() == 0){
                removeKeys.add(attrSection);
            }
        }
        for(String remove : removeKeys){
            attrSections.remove(remove);
        }

        return attrSections;
    }

    public String decode(String input){
        try {

            Base64.Decoder decoder = Base64.getDecoder();
            byte[] decodedByte = decoder.decode(input);
            String decodedString = new String(decodedByte);
            return decodedString;
        }
        catch (Exception e) {
            System.out.println("Exception thrown"
                    + " for incorrect algorithm: " + e);

            return null;
        }
    }

    public LinkedHashMap initializeAttrSections(){
        LinkedHashMap<String,List<CategoryAttributeDTO>> attrSections = new LinkedHashMap<String,List<CategoryAttributeDTO>>();
        attrSections.put("General",new ArrayList<CategoryAttributeDTO>());
        attrSections.put("Material & Color",new ArrayList<CategoryAttributeDTO>());
        attrSections.put("Operation",new ArrayList<CategoryAttributeDTO>());
        attrSections.put("Maintenance",new ArrayList<CategoryAttributeDTO>());
        attrSections.put("Dimensions",new ArrayList<CategoryAttributeDTO>());
        attrSections.put("Warranty",new ArrayList<CategoryAttributeDTO>());
        return attrSections;
    }

    public LinkedHashMap initializeItemAttrSections(){
        LinkedHashMap<String,List<ItemAttributeValueDTO>> attrSections = new LinkedHashMap<String,List<ItemAttributeValueDTO>>();
        attrSections.put("General",new ArrayList<ItemAttributeValueDTO>());
        attrSections.put("Material & Color",new ArrayList<ItemAttributeValueDTO>());
        attrSections.put("Operation",new ArrayList<ItemAttributeValueDTO>());
        attrSections.put("Maintenance",new ArrayList<ItemAttributeValueDTO>());
        attrSections.put("Dimensions",new ArrayList<ItemAttributeValueDTO>());
        attrSections.put("Warranty",new ArrayList<ItemAttributeValueDTO>());
        return attrSections;
    }

    public LoadItemDTO loadItemDetails(List queryResults){
        LoadItemDTO loadItemDTO = new LoadItemDTO();
        LinkedHashMap<String,List<ItemAttributeValueDTO>> attrSections = initializeItemAttrSections();
        Integer categoryId = null;
        if(null != queryResults){
            for(Object row : queryResults){
                Object[] objects = null;
                System.out.println(" in row : " + row);
                if (row.getClass().isArray()) {
                    objects = (Object[]) row;
                }
                loadItemDTO.setId(Integer.parseInt(objects[0].toString()));
                loadItemDTO.setName((objects[1].toString()));
                loadItemDTO.setGeometryLink((objects[2].toString()));
                loadItemDTO.setThumbnailLink((objects[3].toString()));
                ItemAttributeValueDTO itemAttributeValueDTO = new ItemAttributeValueDTO();
                itemAttributeValueDTO.setItemId(Integer.parseInt(objects[0].toString()));
                itemAttributeValueDTO.setAttrId(Integer.parseInt(objects[6].toString()));
                itemAttributeValueDTO.setAttrName(objects[5].toString());
                itemAttributeValueDTO.setAttrValue(objects[7].toString());
                itemAttributeValueDTO.setAttributeType(objects[10].toString());
                if(attrSections.keySet().contains(objects[8].toString())){
                    List<ItemAttributeValueDTO> existingList = attrSections.get(objects[8].toString());
                    existingList.add(itemAttributeValueDTO);
                    attrSections.put(objects[8].toString(),existingList);
                }
                else {
                    List<ItemAttributeValueDTO> newList = new ArrayList<ItemAttributeValueDTO>();
                    newList.add(itemAttributeValueDTO);
                    attrSections.put(objects[8].toString(),newList);
                }
                categoryId = Integer.parseInt(objects[9].toString());
                loadItemDTO.setCategoryId(categoryId);
            }
            List<String> removeKeys = new ArrayList<>();
            for(String attrSection : attrSections.keySet()){
                if(((List)attrSections.get(attrSection)).size() == 0){
                    removeKeys.add(attrSection);
                }
            }
            for(String remove : removeKeys){
                attrSections.remove(remove);
            }
        }
        loadItemDTO.setItemAttributes(attrSections);
        StringBuffer categoryString = new StringBuffer();
        CategoryDef categoryDef = categoryDefRepository.findById(categoryId).get();
        categoryString.append(categoryDef.getName());
        Integer parentId = categoryDef.getParentId();
        while(parentId != null ){
            categoryDef = categoryDefRepository.findById(parentId).get();
            categoryString.insert(0," > ");
            categoryString.insert(0, categoryDef.getName());
            parentId = categoryDef.getParentId();
        }
        loadItemDTO.setCategoryName(categoryString.toString());
        return loadItemDTO;
    }

    public Object makeGETAPICall(String uri, Map<String,String> parameters){

        StringBuffer params = new StringBuffer();
        if(null != parameters){
            if(parameters.keySet().size() > 0 ) params.append("?");
            for(String key : parameters.keySet()){
                params.append(key).append("=").append(parameters.get(key));
                params.append("&");
            }
            uri = uri + params.toString().substring(0, params.lastIndexOf("&"));
        }

        String result = restTemplate.getForObject(uri, String.class);
        return result;
    }

    public String convertSkpFileToTzi(String geometryFileLink){
        String uri = "https://skptotzieight120190308040245.azurewebsites.net/api/func64bit1?code=38ZtVnrcH/amkniNlCUwukGziaALuRhlLUhr07JbuA8AJazaCsdAJw==";
        StringBuffer finalUri = new StringBuffer();
        String storageKey = "AUAMGwsj5oXP8Tc7NdX5qY5QjOHZ5pCuVY9NrnBqubEkOiTwoi9hFpSyqQ1X53k7lqoTUDtgIqqg9P+7wwSZBw==";
        String newStorage = storageKey.replace("+", "%2b");
        finalUri.append(uri);
        finalUri.append("&").append("inputFile=").append(geometryFileLink.substring(0, geometryFileLink.indexOf("?"))).append("&").append("storageKey=").append(newStorage);
        String result = (String)makeGETAPICall(finalUri.toString(),null);
        String tziLink = result.substring(result.indexOf(", ") + 2);
        return tziLink;
    }

    public List<FilterDTO> populateFilters(List queryResults, Integer clientId){
        List<FilterDTO> filters = new ArrayList<FilterDTO>();
        if(null != queryResults) {
            for (Object row : queryResults) {
                Object[] objects = null;
                System.out.println(" in row : " + row);
                if (row.getClass().isArray()) {
                    objects = (Object[]) row;
                }
                FilterDTO filterDTO = new FilterDTO();
                filterDTO.setId(Integer.parseInt(objects[0].toString()));
                filterDTO.setName((objects[1].toString()));
                filterDTO.setCount(new Integer(0));
                List distincts = itemRepository.getDistinctValuesPerFilterAttribute(Integer.parseInt(objects[0].toString()));
                List<FilterItemDTO> filterItemDTOS = new ArrayList<FilterItemDTO>();
                for(Object dRow : distincts){
                    Object[] dObj = null;
                    System.out.println(" in row : " + dObj);
                    if (dRow.getClass().isArray()) {
                        dObj = (Object[]) dRow;
                    }
                    FilterItemDTO filterItemDTO = new FilterItemDTO();
                    filterItemDTO.setName(dObj[0].toString());
                    filterItemDTO.setCount(Integer.parseInt(dObj[1].toString()));
                    filterItemDTO.setUrl("/item/filter?clientId=" + clientId + "&type=attribute&key=" + Integer.parseInt(objects[0].toString())
                            + "&value=" + dObj[0].toString());
                    filterItemDTOS.add(filterItemDTO);
                    filterDTO.setCount(filterDTO.getCount() + Integer.parseInt(dObj[1].toString()));
                }
                filterDTO.setFilterItems(filterItemDTOS);
                filters.add(filterDTO);
            }
        }
        return filters;
    }
}
