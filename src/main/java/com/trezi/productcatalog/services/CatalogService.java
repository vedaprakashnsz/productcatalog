package com.trezi.productcatalog.services;

import com.trezi.productcatalog.domain.CategoryDef;
import com.trezi.productcatalog.domain.DraftItem;
import com.trezi.productcatalog.domain.Item;
import com.trezi.productcatalog.domain.ItemAttrValues;
import com.trezi.productcatalog.dto.*;
import com.trezi.productcatalog.exception.CatalogException;
import com.trezi.productcatalog.exception.TZIConversionException;
import com.trezi.productcatalog.repository.*;
import com.trezi.productcatalog.utility.ConvertorServiceFeignProxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.inject.Inject;
import java.util.*;

@Service
public class CatalogService {

    private static final Logger logger = LoggerFactory.getLogger(CatalogService.class.getName());

    @Inject
    CategoryDefRepository categoryDefRepository;

    @Inject
    CategoryAttributeMapRepository categoryAttributeMapRepository;

    @Inject
    UtilityService utilityService;

    @Inject
    DraftItemRepository draftItemRepository;

    @Inject
    BlobService blobService;

    @Inject
    ItemRepository itemRepository;

    @Inject
    ItemAttrValuesRepository itemAttrValuesRepository;

    @Autowired
    ConvertorServiceFeignProxy convertorServiceFeignProxy;

    public CategoryDef getCategoryById(Integer id){
        try{
            CategoryDef catalogCategory = categoryDefRepository.findById(id).get();
            return catalogCategory;
        }
        catch (Exception e){
            logger.error(" Error in getting category definition : " + e.getMessage());
            throw new CatalogException("Error in fetching category.");
        }
    }

    public List<CategoryDef> getCategories(){
        try{
            List<CategoryDef> categories = categoryDefRepository.findAll();
            return categories;
        }
        catch (Exception e){
           logger.error(" Error in getting categories : " + e.getMessage());
           throw new CatalogException(" Error in fetching categories");
        }
    }

    public List getCategoryCounts(Integer clientId){
        try{
            List counts = itemRepository.getCounts(clientId);
            return counts;
        }
        catch (Exception e){
            logger.error(" Error in getting category counts : " + e.getMessage());
            throw new CatalogException(" Error in fetching category counts ");
        }
    }

    public Map getCategoryDataById(Integer categoryId){
//        List<CategoryAttributeMap> categoryAttributes = categoryAttributeMapRepository.findAllByCatId(categoryId);
        List catalogAttributes = categoryAttributeMapRepository.fetchAtrributeInfoByCategory(categoryId);
        Map<String,List<CategoryAttributeDTO>> categoryAttributeDTOS = utilityService.populateCatalogAttributes(catalogAttributes);
        System.out.println(" tehe list : " + catalogAttributes);
        return categoryAttributeDTOS;
    }

    public DraftItem uploadGeometryFile(MultipartFile geometryFile, Integer categoryId, String itemName,
                                        Integer itemId, String clientId, String createdBy){
        try{

            //conversion service health check
            String healthCheck = convertorServiceFeignProxy.healthCheck();
            logger.info("health:",healthCheck);

            DraftItem draftItem = null;
            if(itemId != null){
                draftItem = draftItemRepository.findById(itemId).get();
                if(null == draftItem) throw new CatalogException("Error uploading geometry file. Please try again.");
            }
            else {
                draftItem = new DraftItem();
            }
            draftItem.setCatId(categoryId);
            draftItem.setClientId(clientId);
            draftItem.setCreatedBy(createdBy);
            draftItem.setCreatedOn(new Date());
            draftItem.setName(itemName);
            draftItemRepository.save(draftItem);
            draftItem = draftItemRepository.findById(draftItem.getId()).get();

            blobService.deleteAllChildFolders("temp/" + draftItem.getId() + "/" + "geometry/");
            String geometryLink = blobService.uploadFileToContainer("temp/" + draftItem.getId() + "/" + "geometry/" + geometryFile.getOriginalFilename(), geometryFile);


            //invoke the conversion service based on skp or rvt
            ResponseEntity<?> convertorServiceResponse = null;
            Map data = null;
            String uploadedFileName = geometryFile.getOriginalFilename();
                if (uploadedFileName.endsWith(".skp")) {
                    convertorServiceResponse = convertorServiceFeignProxy.convertSkpToTZI(geometryLink);
                } else if (uploadedFileName.endsWith(".rvt")) {
                    convertorServiceResponse = convertorServiceFeignProxy.convertRvtToTZI(geometryLink);
                }
                Map response = (Map) convertorServiceResponse.getBody();
                data = (Map) response.get("data");
                logger.info(response.toString());
                if(!(data.containsKey("tziFile"))){
                    throw new TZIConversionException("Failed in TZI file conversion.");
                }

            draftItem.setGeometryLink((String) data.get("tziFile"));
            draftItemRepository.save(draftItem);
            return draftItem;
        }
        catch (Exception e){
            logger.error("Error in creating Draft Item : " + e);
            throw new CatalogException("Error in uploading geomtery file");
        }
    }

    public DraftItem uploadThumbnail(MultipartFile thumbnailFile, Integer categoryId, String itemName,
                                        Integer itemId, String clientId, String createdBy){
        try{
            DraftItem draftItem = null;
            if(itemId != null){
                draftItem = draftItemRepository.findById(itemId).get();
                if(null == draftItem) throw new CatalogException("Error uploading thumbnail file. Please try again.");
            }
            else {
                draftItem = new DraftItem();
            }
            draftItem.setCatId(categoryId);
            draftItem.setClientId(clientId);
            draftItem.setCreatedBy(createdBy);
            draftItem.setCreatedOn(new Date());
            draftItem.setName(itemName);
            draftItemRepository.save(draftItem);
            draftItem = draftItemRepository.findById(draftItem.getId()).get();
            blobService.deleteAllChildFolders("temp/" + draftItem.getId() + "/" + "thumbnail/");
            String thumbnailLink = blobService.uploadFileToContainer("temp/" + draftItem.getId() + "/" + "thumbnail/" + thumbnailFile.getOriginalFilename(), thumbnailFile);
            draftItem.setThumbnailLink(thumbnailLink);
            draftItemRepository.save(draftItem);
            return draftItem;
        }
        catch (Exception e){
            logger.error("Error in creating Draft Item : " + e.getMessage());
            throw new CatalogException("Error in uploading thumbnail file");
        }
    }

    @Transactional
    public Item saveItem(SaveEditItemDTO saveEditItemDTO){
        Item item = null;
        try{
            Integer id = saveEditItemDTO.getItemId();
            Item baseItem = itemRepository.findById(id).get();
            DraftItem draftItem = draftItemRepository.findById(id).get();
            if(null != baseItem){
                item = baseItem;
            }
            else if(null != draftItem){
                item = new Item();
                copyDraftIntoItem(draftItem, item);
                String newThumbnailLink = blobService.copyBlobs("temp/" + draftItem.getId() + "/" + "thumbnail/",item.getId()+"/thumbnail/");
                String newGeometryLink = blobService.copyBlobs("temp/" + draftItem.getId() + "/" + "geometry/",item.getId()+"/geometry/");
                item.setGeometryLink(newGeometryLink);
                item.setThumbnailLink(newThumbnailLink);
                itemRepository.save(item);
            }
            else{
                throw new CatalogException("Error saving catalog item, no reference exists");
            }
            item.setName(saveEditItemDTO.getName());
            item.setCatId(Integer.parseInt(saveEditItemDTO.getCatId()));
            itemRepository.save(item);

            item = itemRepository.findById(item.getId()).get();
            Integer deleted = itemAttrValuesRepository.deleteItemAttriButes(item.getId());
            createItemAttributeValues(item, saveEditItemDTO.getItemAttributeValueDTOS());
            deleteDraftItem(draftItem);
            return item;
        }
        catch (Exception e){
            logger.error("Error in saving item : " + e.getMessage());
            throw new CatalogException("Error saving catalog item");
        }
    }

    public void copyDraftIntoItem(DraftItem draftItem, Item item){
        item.setName(draftItem.getName());
        item.setClientId(Integer.parseInt(draftItem.getClientId()));
        item.setCatId(draftItem.getCatId());
        item.setActive(true);
        item.setPublished(false);
        item.setCreatedOn(new Date());
        item.setCreatedBy(draftItem.getCreatedBy());
        item.setEditedBy(draftItem.getEditedBy());
        item.setEditedOn(new Date());
    }

    public Boolean createItemAttributeValues(Item item, List<ItemAttributeValueDTO> itemAttributeValueDTOS){
        try{
            List<ItemAttrValues> itemAttrValues = new ArrayList<ItemAttrValues>();
            for (ItemAttributeValueDTO itemAttributeValueDTO : itemAttributeValueDTOS){
                ItemAttrValues itemAttrs = new ItemAttrValues();
                itemAttrs.setAttrId(itemAttributeValueDTO.getAttrId());
                itemAttrs.setAttrValue(itemAttributeValueDTO.getAttrValue());
                itemAttrs.setItemId(item.getId());
                itemAttrValues.add(itemAttrs);
            }
            itemAttrValuesRepository.saveAll(itemAttrValues);
        }
        catch (Exception e){
            logger.error("Error saving item attributes : " + e.getMessage());
            throw new CatalogException("Error saving item attributes");

        }
        return true;
    }

    public List<ListItemDTO> getItemsList(Integer clientId, Boolean isPublished){
        List<ListItemDTO> listItems = new ArrayList<ListItemDTO>();
        try{
            Iterable<Item> items = itemRepository.findAllByClientId(clientId);
            for(Item item :items){
                if(null != isPublished && item.getPublished() == isPublished){
                    ListItemDTO listItemDTO = new ListItemDTO();
                    listItemDTO.setId(item.getId());
                    listItemDTO.setName(item.getName());
                    listItemDTO.setGeometryLink(item.getGeometryLink());
                    listItemDTO.setThumbnailLink(item.getThumbnailLink());
                    listItemDTO.setCreatedBy(item.getCreatedBy());
                    listItemDTO.setCreatedOn(item.getCreatedOn().getTime());
                    listItemDTO.setPublished(item.getPublished());
                    listItems.add(listItemDTO);
                }
                else if(null == isPublished){
                    ListItemDTO listItemDTO = new ListItemDTO();
                    listItemDTO.setId(item.getId());
                    listItemDTO.setName(item.getName());
                    listItemDTO.setGeometryLink(item.getGeometryLink());
                    listItemDTO.setThumbnailLink(item.getThumbnailLink());
                    listItemDTO.setCreatedBy(item.getCreatedBy());
                    listItemDTO.setCreatedOn(item.getCreatedOn().getTime());
                    listItemDTO.setPublished(item.getPublished());
                    listItems.add(listItemDTO);
                }
            }
        }
        catch (Exception e){
            logger.error("Error fetching items list : " + e.getMessage());
            throw new CatalogException("Error getting list of items.");
        }
        return listItems;
    }

    public List<ListItemDTO> getItemsListByCategoryId(Integer clientId, String catId){
        List<ListItemDTO> listItems = new ArrayList<ListItemDTO>();
        List<Integer> catIds = new ArrayList<Integer>();
        if(catId.contains(",")){
            String[] catSplit = catId.split(",");
            for(String id : catSplit){
                catIds.add(Integer.parseInt(id));
            }
        }
        else {
            catIds.add(Integer.parseInt(catId));
        }
        try{
            Iterable<Item> items = itemRepository.findAllByClientIdAndCatId(clientId, catIds);
            for(Item item :items){
                ListItemDTO listItemDTO = new ListItemDTO();
                listItemDTO.setId(item.getId());
                listItemDTO.setName(item.getName());
                listItemDTO.setGeometryLink(item.getGeometryLink());
                listItemDTO.setThumbnailLink(item.getThumbnailLink());
                listItemDTO.setCreatedBy(item.getCreatedBy());
                listItemDTO.setCreatedOn(item.getCreatedOn().getTime());
                listItemDTO.setPublished(item.getPublished());
                listItems.add(listItemDTO);

            }
        }
        catch (Exception e){
            logger.error("Error fetching items list : " + e.getMessage());
            throw new CatalogException("Error getting list of items.");
        }
        return listItems;
    }

    public List<ListItemDTO> getItemsListByAttributes(Integer clientId, Integer attrId, String attrValue){
        List<ListItemDTO> listItems = new ArrayList<ListItemDTO>();
        List<String> attrValues = new ArrayList<String>();
        try{
            if(attrValue.contains(",")){
                String[] attrArr = attrValue.split(",");
                attrValues = Arrays.asList(attrArr);
            }
            else {
                attrValues.add((attrValue));
            }
            Iterable<Item> items = itemRepository.getAllByAttrTypeAndValue(clientId, attrId, attrValues);
            for(Object obj :items){
                Item item = (Item) obj;
                ListItemDTO listItemDTO = new ListItemDTO();
                listItemDTO.setId(item.getId());
                listItemDTO.setName(item.getName());
                listItemDTO.setGeometryLink(item.getGeometryLink());
                listItemDTO.setThumbnailLink(item.getThumbnailLink());
                listItemDTO.setCreatedBy(item.getCreatedBy());
                listItemDTO.setCreatedOn(item.getCreatedOn().getTime());
                listItemDTO.setPublished(item.getPublished());
                listItems.add(listItemDTO);

            }
        }
        catch (Exception e){
            logger.error("Error fetching items list : " + e.getMessage());
            throw new CatalogException("Error getting list of items.");
        }
        return listItems;
    }

    public void deleteDraftItem(DraftItem draftItem){
        try{
            blobService.deleteAllChildFolders("temp/" + draftItem.getId() + "/" + "geometry/");
            blobService.deleteAllChildFolders("temp/" + draftItem.getId() + "/" + "thumbnail/");
            draftItemRepository.delete(draftItem);
        }
        catch (Exception e){
            logger.error("Error deleting draft item : " + e.getMessage());
            throw new CatalogException("Error deleting draft item");
        }
    }

    public Boolean publishItem(PublishItemDTO publishItemDTO){
        try{
            Integer itemId = publishItemDTO.getItemId();
            Item item = itemRepository.findById(itemId).get();
            item.setPublished(publishItemDTO.getPublished());
            itemRepository.save(item);
        }
        catch (Exception e){
            logger.error("Error is publishing item with id " + publishItemDTO.getItemId() + " message : " + e.getMessage());
            throw new CatalogException("Error in publishing the item");
        }
        return true;
    }

    public LoadItemDTO getItemDetails(Integer itemId){
        try{
            List itemsList = itemRepository.fetchItemDetails(itemId);
            LoadItemDTO loadItemDTO = utilityService.loadItemDetails(itemsList);
            return loadItemDTO;
        }
        catch (Exception e){
            logger.error("error in fetching the item : " + e.getMessage());
            throw new CatalogException("Error in fetching item details");
        }
    }

    @Transactional
    public Boolean deleteItem(Integer itemId){
        try{
            Item item = itemRepository.findById(itemId).get();
            if(null == item) throw new CatalogException("No item exists with the given id");
            blobService.deleteAllChildFolders(item.getId() + "/" + "geometry/");
            blobService.deleteAllChildFolders(item.getId() + "/" + "thumbnail/");
            Integer deleted = itemAttrValuesRepository.deleteItemAttriButes(itemId);
            itemRepository.delete(item);
            return true;
        }
        catch (Exception e){
            logger.error("Error in deleting item : " + e.getMessage());
            throw new CatalogException("Error while deleting item");
        }
    }

    public List getFilters(Integer clientId, Integer catId){
        List<FilterDTO> filters = new ArrayList<FilterDTO>();
        List filterList = null;
        try{
            if(null != catId && catId > 0){
                filterList = itemRepository.getFilters(catId);
            }
            else {
                filterList = itemRepository.getFilters();
            }
            filters = utilityService.populateFilters(filterList, clientId);
        }
        catch (Exception e){
            logger.error("Error in getting filters: " + e.getMessage());
            throw new CatalogException("Error in generating filters");
        }
        return filters;
    }
}
