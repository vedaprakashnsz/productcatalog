package com.trezi.productcatalog.services;

import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.blob.*;
import com.trezi.productcatalog.exception.CatalogException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

@Service
public class BlobService {

    @Value(("${azure.account.name}"))
    private String azureAccountName;

    @Value(("${azure.account.pswd}"))
    private String azurePassword;

    @Value(("${azure.container}"))
    private String azureContainer;

    @Value(("${conversion.account.name}"))
    private String conversionAccountName;

    @Value(("${conversion.account.pswd}"))
    private String conversionPassword;

    @Value(("${conversion.container}"))
    private String conversionContainer;

    private static final Logger logger = LoggerFactory.getLogger(BlobService.class.getName());
    public CloudBlobContainer connectToAzureBlobContainer(String azureAccount, String azurePassword, String azureContainer){
        CloudBlobContainer cloudBlobContainer = null;
        try{
            String storageConnectionString =
                    "DefaultEndpointsProtocol=https;" +
                            "AccountName=" + azureAccount + ";" +
                            "AccountKey=" + azurePassword;
            CloudStorageAccount cloudStorageAccount = CloudStorageAccount.parse(storageConnectionString);
            CloudBlobClient cloudBlobClient = cloudStorageAccount.createCloudBlobClient();
            cloudBlobContainer = cloudBlobClient.getContainerReference(azureContainer);
        }
        catch (Exception e){
            logger.error(e.getMessage());
        }
        return cloudBlobContainer;
    }

    public String uploadFileToContainer(String fileName, MultipartFile file){
        String blobUrl = null;
        try{
            CloudBlobContainer cloudBlobContainer = connectToAzureBlobContainer(azureAccountName,azurePassword,azureContainer);
            if(null != cloudBlobContainer){
                CloudBlockBlob blob = cloudBlobContainer.getBlockBlobReference(fileName);
                blob.upload(file.getInputStream(), file.getSize());
                String policyName = "Policy_"+file.getOriginalFilename();
                String sasToken = generateSASToken(cloudBlobContainer);
                blobUrl = blob.getUri() + "?" + sasToken;
            }
        }
        catch(Exception e){
            logger.error(e.getMessage());
        }
        return blobUrl;
    }

    public void deleteAllChildFolders(String parentName){
        try {
            CloudBlobContainer cloudBlobContainer = connectToAzureBlobContainer(azureAccountName, azurePassword, azureContainer);
            if (null != cloudBlobContainer) {
                Iterable<ListBlobItem> blobItems = cloudBlobContainer.listBlobs(parentName, true);
                for (ListBlobItem blob : blobItems) {
                    if (null != blob) {
                        String blobName = blobNameFromUri(blob.getUri());
                        deleteBlob(cloudBlobContainer, blobName);
                    }
                }
            }
        }
        catch (Exception e){
            logger.error("Error in deleting sub folders : " + e.getMessage());
        }
    }

    public String generateSASToken(CloudBlobContainer cloudBlobContainer){
        String sas = null;
        try{
            // Create a new shared access policy.
            SharedAccessBlobPolicy sasPolicy = new SharedAccessBlobPolicy();

            // Create a UTC Gregorian calendar value.
            GregorianCalendar calendar = new GregorianCalendar(
                    TimeZone.getTimeZone("UTC"));

            // Specify the current time as the start time for the shared access
            // signature.
            //
            calendar.setTime(new Date());
            sasPolicy.setSharedAccessStartTime(calendar.getTime());

            // Use the start time delta one hour as the end time for the shared
            // access signature.
            calendar.add(Calendar.YEAR, 1);
            sasPolicy.setSharedAccessExpiryTime(calendar.getTime());


            // Set READ permissions
            sasPolicy.setPermissions(EnumSet.of(
                    SharedAccessBlobPermissions.READ,
                    SharedAccessBlobPermissions.LIST));


            // Create the container permissions.
            BlobContainerPermissions containerPermissions = new BlobContainerPermissions();

            // Turn public access to the container off.
            containerPermissions.setPublicAccess(BlobContainerPublicAccessType.OFF);

            cloudBlobContainer.uploadPermissions(containerPermissions);

            // Create a shared access signature for the container.
            sas = cloudBlobContainer.generateSharedAccessSignature(sasPolicy, null);
            // HACK: when the just generated SAS is used straight away, we get an
            // authorization error intermittently. Sleeping for 1.5 seconds fixes that
            // on my box.
            Thread.sleep(1500);

            // Return to caller with the shared access signature.
            return sas;
        }
        catch (Exception e){
            logger.error(e.getMessage());
        }
        return sas;
    }

    public static String blobNameFromUri(URI uri) {
        String path = uri.getPath();

        // We remove the container name from the path
        // The 3 magic number cames from the fact if path is /container/path/to/myfile
        // First occurrence is empty "/"
        // Second occurrence is "container
        // Last part contains "path/to/myfile" which is what we want to get
        String[] splits = path.split("/", 3);

        // We return the remaining end of the string
        return splits[2];
    }

    public void deleteBlob(CloudBlobContainer blobContainer, String blob) throws URISyntaxException, StorageException {
        if (blobContainer.exists()) {
            CloudBlockBlob azureBlob = blobContainer.getBlockBlobReference(blob);
            azureBlob.deleteIfExists();
        }
    }

    public String copyBlobs(String sourcePath, String destinationPath){
        String newBlobUrl = null;
        try {
            CloudBlobContainer cloudBlobContainer = connectToAzureBlobContainer(azureAccountName, azurePassword, azureContainer);
            if (null != cloudBlobContainer) {
                Iterable<ListBlobItem> blobItems = cloudBlobContainer.listBlobs(sourcePath, true);
                for (ListBlobItem blob : blobItems) {
                    if (null != blob) {
                        CloudBlockBlob blob1 = (CloudBlockBlob)blob;
                        logger.info(blob1.getName());
                        String blobName = blobNameFromUri(blob1.getUri());
                        String blobActualName = blobName.substring(blobName.lastIndexOf("/") + 1);
                        CloudBlockBlob blob2 = cloudBlobContainer.getBlockBlobReference(destinationPath + blobActualName);
                        String copyId = blob2.startCopy(blob1);
                        System.out.println(" the copyId : " + copyId);
                        String policyName = "Policy_"+blobActualName;
                        String sasToken = generateSASToken(cloudBlobContainer);
                        newBlobUrl = blob2.getUri() + "?" + sasToken;
                    }
                }
            }
        }
        catch (Exception e){
            logger.error("Error while copying : " + e.getMessage());
            throw new CatalogException("Error while copying blobs ");
        }
        return newBlobUrl;
    }

//    public String copyBlobForConversion(String sourcePath, String destinationPath){
//        String newBlobUrl = null;
//        try {
//            CloudBlobContainer cloudBlobContainer = connectToAzureBlobContainer(azureAccountName, azurePassword, azureContainer);
//            CloudBlobContainer cloudBlobConversionContainer = connectToAzureBlobContainer(conversionAccountName, conversionPassword, conversionContainer);
//            if (null != cloudBlobContainer && null != cloudBlobConversionContainer) {
//                Iterable<ListBlobItem> blobItems = cloudBlobContainer.listBlobs(sourcePath, true);
//                for (ListBlobItem blob : blobItems) {
//                    if (null != blob) {
//                        CloudBlockBlob blob1 = (CloudBlockBlob)blob;
//                        //blob.upload(file.getInputStream(), file.getSize());
//                        logger.info(blob1.getName());
//                        String blobName = blobNameFromUri(blob1.getUri());
//                        String blobActualName = blobName.substring(blobName.lastIndexOf("/") + 1);
//                        CloudBlockBlob blob2 = cloudBlobConversionContainer.getBlockBlobReference(destinationPath + blobActualName);
//                        String copyId = blob2.startCopy(blob1);
//                        System.out.println(" the copyId : " + copyId);
//                        String policyName = "Policy_"+blobActualName;
//                        String sasToken = generateSASToken(cloudBlobContainer);
//                        newBlobUrl = blob2.getUri() + "?" + sasToken;
//                    }
//                }
//            }
//        }
//        catch (Exception e){
//            logger.error("Error : " + e.getMessage());
//        }
//        return newBlobUrl;
//    }

    public String generateDownloadLink(){
        String sasToken = null;
        try {
            CloudBlobContainer cloudBlobContainer = connectToAzureBlobContainer(azureAccountName, azurePassword, azureContainer);
            if (null != cloudBlobContainer) {
                sasToken = generateSASToken(cloudBlobContainer);
            }
        }
        catch (Exception e){
            logger.error("Error while copying : " + e.getMessage());
            throw new CatalogException("Error while copying blobs ");
        }
        return sasToken;
    }
}
